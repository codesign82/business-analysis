<?php
/**
 * Plugin Name: businessanalysis
 * Plugin URI: https://codesign82.com/businessanalysis
 * Description: analysis data
 * Version: 1.0
 * Author: Codesign
 * Author URI: https://codesign82.com/
 */

/*************************************
 * ADD SCRIPTS & STYLES
 **************************************/
function businessanalysis_enqueue_script() {
//  wp_enqueue_script('gsap', plugin_dir_url(__FILE__) . '/assets/js/gsap.min.js');
//  wp_enqueue_script('scripts', plugin_dir_url(__FILE__) . '/assets/js/scripts.js',['gsap','draw-svg']);
//  wp_enqueue_script('draw-svg', plugin_dir_url(__FILE__) . '/assets/js/drawSVGPlugin.js',['gsap']);
  wp_enqueue_script('wp-util');

//  wp_enqueue_style('styles', plugin_dir_url(__FILE__) . '/assets/style/style.css');
}
add_action('wp_enqueue_scripts', 'businessanalysis_enqueue_script');


/**
 * Add "Custom" template to page attirbute template section.
 */
function buyability_add_template_to_select($post_templates, $wp_theme, $post, $post_type)
{

    // Add custom template named businessanalysis-template.php to select dropdown
    $post_templates['businessanalysis-template.php'] = __('Business Analysis Template');

    return $post_templates;
}

add_filter('theme_page_templates', 'buyability_add_template_to_select', 10, 4);

/**
 * Check if current page has our custom template. Try to load
 * template from theme directory and if not exist load it
 * from root plugin directory.
 */
function buyability_load_plugin_template($template)
{

    if (get_page_template_slug() === 'businessanalysis-template.php') {

        if ($theme_file = locate_template(array('businessanalysis-template.php'))) {
            $template = $theme_file;
        } else {
            $template = plugin_dir_path(__FILE__) . 'businessanalysis-template.php';
        }
    }

    if ($template == '') {
        throw new \Exception('No template found');
    }

    return $template;
}

add_filter('template_include', 'buyability_load_plugin_template');


function buyability_custom_post_type()
{
    // Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Questions', 'Post Type General Name', 'business-analysis'),
        'singular_name' => _x('Question', 'Post Type Singular Name', 'business-analysis'),
        'menu_name' => __('Questions', 'business-analysis'),
        'parent_item_colon' => __('Parent Question', 'business-analysis'),
        'all_items' => __('All Questions', 'business-analysis'),
        'view_item' => __('View Question', 'business-analysis'),
        'add_new_item' => __('Add New Question', 'business-analysis'),
        'add_new' => __('Add New Question', 'business-analysis'),
        'edit_item' => __('Edit Question', 'business-analysis'),
        'update_item' => __('Update Question', 'business-analysis'),
        'search_items' => __('Search Question', 'business-analysis'),
        'not_found' => __('Question Not Found', 'business-analysis'),
        'not_found_in_trash' => __('Question Not found in Trash', 'business-analysis'),
    );

    // Set other options for Custom Post Type

    $args = array(
        'label' => __('question', 'business-analysis'),
        'description' => __('Question', 'business-analysis'),
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array(
            'title',
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'menu_icon' => 'dashicons-smiley',
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        //      'rewrite'             => array('slug' => 'icon'),
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    // Registering your Custom Post Type
    register_post_type('questions', $args);

}

add_action('init', 'buyability_custom_post_type', 0);

function buyability_add_custom_taxonomies()
{
    // Add new "Question Categories" taxonomy to Posts

    register_taxonomy('questions_categories',
        'questions',
        array(// Hierarchical taxonomy (like categories)
            'hierarchical' => true,
            // This array of options controls the labels displayed in the WordPress Admin UI
            'labels' => array(
                'name' => _x('Question Categories', 'taxonomy general name', 'business-analysis'),
                'singular_name' => _x('Question Category', 'taxonomy singular name', 'business-analysis'),
                'search_items' => __('Search Question Categories', 'business-analysis'),
                'all_items' => __('All Question Categories', 'business-analysis'),
                'parent_item' => __('Parent Question Category', 'business-analysis'),
                'parent_item_colon' => __('Parent Question Category:', 'business-analysis'),
                'edit_item' => __('Edit Question Category', 'business-analysis'),
                'update_item' => __('Update Question Category', 'business-analysis'),
                'add_new_item' => __('Add New Question Category', 'business-analysis'),
                'new_item_name' => __('New Question Category Name', 'business-analysis'),
                'menu_name' => __('Question Categories', 'business-analysis'),
            ),
            'show_in_rest' => true,
            'posts_per_page' => -1,
            // Control the slugs used for this taxonomy
            'rewrite' => false,
            'public' => true,
        ));
}

add_action('init', 'buyability_add_custom_taxonomies', 10);


/* AJAX Callback */
add_action('wp_ajax_send_results_email', function () {
    if (!isset($_POST['results'])) {
        wp_send_json_error([$_POST['results'], $_POST['email']]);
    }
    $results = $_POST['results'];
    $email = $_POST['email'];
    $from = get_option('admin_email');
// To send HTML mail, the Content-type header must be set
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Create email headers
    $headers .= 'From: ' . $from . "\r\n" .
        'Reply-To: ' . $from . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $message = "
<H1>Buyability Assessment Results</H1>
<H2>Welcome $email</H2>

<table style='max-width: 80%; margin: 0 auto 0 0; border-collapse: collapse;'>
<thead>
  <tr>
    <th style='padding: 10px 20px;font-size: 20px; text-align:left; border: black solid 1px;'>Category</th>
    <th style='padding: 10px 20px;font-size: 20px; text-align:center; border: black solid 1px;'>Result</th>
  </tr>
</thead>
<tbody>
";
    foreach ($results as $key => $value) {
        $message .= "
  <tr>
    <th style='padding: 10px 20px;font-size: 20px; text-align:left; border: black solid 1px;'>".ucfirst(strtolower($key))."</th>
    <td style='padding: 10px 20px;font-size: 20px; text-align:center; border: black solid 1px;'>$value</td>
  </tr>
  ";
    }

    $message .= '
</tbody>
</table>';
    wp_mail($email, 'Buyability Assessment Results', $message, $headers);
    wp_send_json_success(true);
});


add_action( 'wp_ajax_save_question_for_users', 'save_question_for_users' );
add_action( 'wp_ajax_nopriv_save_question_for_users', 'save_question_for_users' );
function save_question_for_users() {
  global $current_user;
  if ( isset( $_POST['page'] ) && isset( $_POST['question'] ) ) {
    if(is_user_logged_in() && $_POST['active_circles'] != 0){
    $json = json_decode(get_user_meta($current_user->ID,$_POST['page'].'_question',true),true);
    $json[$_POST['question']] = $_POST['active_circles'];
      update_user_meta($current_user->ID,$_POST['page'].'_question',json_encode($json));
      //echo 'saved';
    }
  }
  exit;
}


add_action( 'wp_ajax_clear_questions', 'clear_questions' );
add_action( 'wp_ajax_nopriv_clear_questions', 'clear_questions' );
function clear_questions() {
  global $current_user;
  if ( isset( $_POST['page'] )) {
    if(is_user_logged_in()){
    update_user_meta($current_user->ID,$_POST['page'].'_question','');
    }
  }
  exit;
}


add_action( 'wp_ajax_get_question_for_users', 'get_question_for_users' );
add_action( 'wp_ajax_nopriv_get_question_for_users', 'get_question_for_users' );
function get_question_for_users() {
  global $current_user;
  if ( isset( $_POST['page'] )) {
    if(is_user_logged_in()){
    $json = get_user_meta($current_user->ID,$_POST['page'].'_question',true);
    echo $json;
    }
  }
  exit;
}

