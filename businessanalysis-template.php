<?php
/*
 * Template Name: Business Analysis Template
 * */
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <link href="<?= plugin_dir_url(__FILE__) . 'assets/styles/style.css' ?>" rel="stylesheet">
  <link href="<?= get_template_directory_uri() . '/style-for-plugins.css' ?>" rel="stylesheet">
  <title><?php the_title(); ?></title>
</head>
<?php wp_head();
$ba_logo = get_field('ba_logo');
$ba_logo_link = get_field('ba_logo_link');
$ba_footer_text = get_field('ba_footer_text');
$ba_facebook_link = get_field('ba_facebook_link');
$ba_twitter_link = get_field('ba_twitter_link');
$ba_linkedin_link = get_field('ba_linkedin_link');
$ba_youtube_link = get_field('ba_youtube_link');
$bg_color = get_field('bg_color');
$svg_logo = get_field('svg_logo');
$bold_logo_text = get_field('bold_logo_text');
$light_logo_text = get_field('light_logo_text');
$form_shortcode = get_field('form_shortcode');
?>
<body>
<style>
  header, .page-wrapper {
    background-color: <?=$bg_color?>;
  }
  
  .page-wrapper .step-wrapper .phases-and-steps .steps .step.secondStep .question-and-evaluation .question {
    height: auto !important;
  }
  
  .page-wrapper .step-wrapper .phases-and-steps .steps .step.secondStep {
    background: transparent;
  }
</style>
<div class="nav b-color">
  <div class="nav-content  container">
    <a href="<?= site_url() ?>" class="steps-move">
      <svg class="small-steps" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
           viewBox="0 0 155 125">
        <title>snake-svg</title>
        <path
            d="M113.67,87.81h35.19A1.12,1.12,0,0,1,150,88.93V123a1.11,1.11,0,0,1-1.11,1.11H113.67V87.81Z"
            fill="none"
            stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M113.67,87.81h35.19A1.12,1.12,0,0,1,150,88.93V123a1.11,1.11,0,0,1-1.11,1.11H113.67Z"
            fill="#fff"/>
        <g style="isolation:isolate">
          <path d="M122.49,99.55l4.83-4.4h1.91V116.4h-3.34V101l-3.4,3.09Z"
                fill="#c32729"/>
          <path
              d="M131.45,116.4v-6a6.37,6.37,0,0,1,2.24-4.92A13.08,13.08,0,0,1,136,104l.59-.33a3.43,3.43,0,0,0,.56-.36,2.64,2.64,0,0,0,1-2.31v-.91a2.06,2.06,0,0,0-.46-1.52,1.73,1.73,0,0,0-1.24-.36,1.81,1.81,0,0,0-1.25.36,2,2,0,0,0-.45,1.52V102h-3.34v-1.85a5.14,5.14,0,0,1,1.51-4,5.63,5.63,0,0,1,7,0,5.15,5.15,0,0,1,1.52,4V101a6.39,6.39,0,0,1-.58,2.68,6.12,6.12,0,0,1-1.67,2.18,9.84,9.84,0,0,1-1.91,1.21,8.08,8.08,0,0,0-1.52,1,3.09,3.09,0,0,0-1.06,2.37v2.61h6.77v3.34Z"
              fill="#c32729"/>
        </g>
        <a class="gray" id="gray">
          <path
              d="M113.67,87.81h35.19A1.12,1.12,0,0,1,150,88.93V123a1.11,1.11,0,0,1-1.11,1.11H113.67Z"
              fill="#fff"/>
        </a>
        <path
            d="M113.66,87.81H77.36v36.3h36.3V108.82h0l2.36-2.36a.71.71,0,0,0,0-1l-2.36-2.36h0V87.81Z"
            fill="none"
            stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M113.66,87.81H77.36v36.3h36.3V108.82l2.36-2.36a.71.71,0,0,0,0-1l-2.36-2.36Z"
            fill="#fff"/>
        <g style="isolation:isolate">
          <path d="M87.57,99.55l4.83-4.4h1.91V116.4H91V101l-3.4,3.09Z"
                fill="#c32729"/>
          <path d="M96.44,99.55l4.82-4.4h1.92V116.4H99.84V101l-3.4,3.09Z"
                fill="#c32729"/>
        </g>
        <a class="gray" id="gray-2" data-name="gray">
          <path
              d="M113.66,87.81H77.36v36.3h36.3V108.82l2.36-2.36a.71.71,0,0,0,0-1l-2.36-2.36Z"
              fill="#fff"/>
        </a>
        <path
            d="M77.38,87.81H41.07v36.3H77.38V108.82h0l2.35-2.36a.71.71,0,0,0,0-1l-2.35-2.36h0V87.81Z"
            fill="none"
            stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M77.38,87.81H41.07v36.3H77.38V108.82l2.35-2.36a.71.71,0,0,0,0-1l-2.35-2.36Z"
            fill="#fff"/>
        <g style="isolation:isolate">
          <path d="M51,99.55l4.83-4.4h1.91V116.4H54.43V101L51,104.07Z"
                fill="#c32729"/>
          <path
              d="M65,116.64a5,5,0,0,1-3.53-1.24,5.26,5.26,0,0,1-1.51-4V100.13a5.26,5.26,0,0,1,1.51-4,5.63,5.63,0,0,1,7,0,5.26,5.26,0,0,1,1.51,4v11.29a5.26,5.26,0,0,1-1.51,4A5,5,0,0,1,65,116.64Zm0-18.39a1.81,1.81,0,0,0-1.25.36,2,2,0,0,0-.45,1.52v11.29a2,2,0,0,0,.45,1.52,1.81,1.81,0,0,0,1.25.36,1.73,1.73,0,0,0,1.24-.36,2,2,0,0,0,.46-1.52V100.13a3.48,3.48,0,0,0-.11-.85,1.38,1.38,0,0,0-.35-.67A1.73,1.73,0,0,0,65,98.25Z"
              fill="#c32729"/>
        </g>
        <a class="gray" id="gray-3" data-name="gray">
          <path
              d="M77.38,87.81H41.07v36.3H77.38V108.82l2.35-2.36a.71.71,0,0,0,0-1l-2.35-2.36Z"
              fill="#fff"/>
        </a>
        <path
            d="M37.41,84.14H1.1a40,40,0,0,0,40,40V108.92h0l2.47-2.46a.71.71,0,0,0,0-1L41.07,103h0V87.81a3.66,3.66,0,0,1-3.66-3.67Z"
            fill="none" stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M37.41,84.14H1.1a40,40,0,0,0,40,40V108.92l2.47-2.46a.71.71,0,0,0,0-1L41.07,103V87.81a3.66,3.66,0,0,1-3.66-3.67Z"
            fill="#fff"/>
        <g style="isolation:isolate">
          <path
              d="M22.75,106.72v1.67a2,2,0,0,0,.45,1.52,1.83,1.83,0,0,0,1.25.36,1.72,1.72,0,0,0,1.24-.36,1.3,1.3,0,0,0,.35-.67,3.54,3.54,0,0,0,.11-.85v-3.76a6.17,6.17,0,0,1-1.7.21,5,5,0,0,1-3.52-1.25,5.12,5.12,0,0,1-1.52-4V97.1a5.27,5.27,0,0,1,1.52-4,5,5,0,0,1,3.52-1.24A5,5,0,0,1,28,93.12a5.27,5.27,0,0,1,1.52,4v11.29a5.27,5.27,0,0,1-1.52,4,5,5,0,0,1-3.52,1.24,5,5,0,0,1-3.52-1.24,5.27,5.27,0,0,1-1.52-4v-1.67ZM26,100.47a3.61,3.61,0,0,0,.11-.85V97.1a3.48,3.48,0,0,0-.11-.85,1.3,1.3,0,0,0-.35-.67,1.72,1.72,0,0,0-1.24-.37,1.83,1.83,0,0,0-1.25.37,2,2,0,0,0-.45,1.52v2.52a2,2,0,0,0,.45,1.51,1.78,1.78,0,0,0,1.25.37,1.67,1.67,0,0,0,1.24-.37A1.31,1.31,0,0,0,26,100.47Z"
              fill="#c32729"/>
        </g>
        <a class="gray" id="gray-4" data-name="gray">
          <path
              d="M37.41,84.14H1.1a40,40,0,0,0,40,40V108.92l2.47-2.46a.71.71,0,0,0,0-1L41.07,103V87.81a3.66,3.66,0,0,1-3.66-3.67Z"
              fill="#fff"/>
        </a>
        <path
            d="M37.41,84.14H23.94a.42.42,0,0,1-.11.15L21.09,87a.71.71,0,0,1-1,0l-2.75-2.74a.58.58,0,0,1-.11-.15H1.11a40,40,0,0,1,40-40V80.48A3.66,3.66,0,0,0,37.41,84.14Z"
            fill="none" stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M37.41,84.14H23.94a.42.42,0,0,1-.11.15L21.09,87a.71.71,0,0,1-1,0l-2.75-2.74a.58.58,0,0,1-.11-.15H1.11a40,40,0,0,1,40-40V80.48A3.66,3.66,0,0,0,37.41,84.14Z"
            fill="#fff"/>
        <g style="isolation:isolate">
          <path
              d="M24.45,78.3a5,5,0,0,1-3.52-1.24,5.27,5.27,0,0,1-1.52-4V71.65a6.18,6.18,0,0,1,2-4.64,6.32,6.32,0,0,1-2-4.68v-.54a5.27,5.27,0,0,1,1.52-4,5,5,0,0,1,3.52-1.25A5,5,0,0,1,28,57.81a5.27,5.27,0,0,1,1.52,4v.42a6.42,6.42,0,0,1-2.1,4.8A6.38,6.38,0,0,1,29,69.18a6.53,6.53,0,0,1,.53,2.6v1.3a5.27,5.27,0,0,1-1.52,4A5,5,0,0,1,24.45,78.3Zm0-18.4a1.83,1.83,0,0,0-1.25.37,2,2,0,0,0-.45,1.52v.54a2.86,2.86,0,0,0,.24,1.2,3.37,3.37,0,0,0,.79,1,8.52,8.52,0,0,0,.67.55l.67-.55a3.2,3.2,0,0,0,.79-1,2.93,2.93,0,0,0,.24-1.23v-.54a2,2,0,0,0-.46-1.52A1.78,1.78,0,0,0,24.45,59.9Zm0,9-.67.46a3.11,3.11,0,0,0-1,2.43v1.3a2,2,0,0,0,.45,1.52,1.83,1.83,0,0,0,1.25.36,1.78,1.78,0,0,0,1.24-.36,2,2,0,0,0,.46-1.52v-1.3a3.07,3.07,0,0,0-1-2.4A3.44,3.44,0,0,0,24.45,68.89Z"
              fill="#c32729"/>
        </g>
        <a class="gray" id="gray-5" data-name="gray">
          <path
              d="M37.41,84.14H23.94a.42.42,0,0,1-.11.15L21.09,87a.71.71,0,0,1-1,0l-2.75-2.74a.58.58,0,0,1-.11-.15H1.11a40,40,0,0,1,40-40V80.48A3.66,3.66,0,0,0,37.41,84.14Z"
              fill="#fff"/>
        </a>
        <path
            d="M77.38,80.48H41.07V65.21h0l-2.39-2.39a.71.71,0,0,1,0-1h0l2.39-2.39h0V44.17H77.38Z"
            fill="none"
            stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M77.38,80.48H41.07V65.21l-2.39-2.39a.71.71,0,0,1,0-1h0l2.39-2.39V44.17H77.38Z"
            fill="#fff"/>
        <g style="isolation:isolate">
          <path d="M64.26,52.31l-5,21.25H55.85L60,55.65H54.18V52.31Z"
                fill="#c32729"/>
        </g>
        <a class="gray" id="gray-6" data-name="gray">
          <path
              d="M77.38,80.48H41.07V65.21l-2.39-2.39a.71.71,0,0,1,0-1h0l2.39-2.39V44.17H77.38Z"
              fill="#fff"/>
        </a>
        <path
            d="M113.66,80.48H77.36V65.21h0L75,62.82a.71.71,0,0,1,0-1h0l2.39-2.39h0V44.17h36.3Z"
            fill="none" stroke="#ccc"
            stroke-miterlimit="20"/>
        <path
            d="M113.66,80.48H77.36V65.21L75,62.82a.71.71,0,0,1,0-1h0l2.39-2.39V44.17h36.3Z"
            fill="#fff"/>
        <g style="isolation:isolate">
          <path
              d="M97.4,58.93V57.29A2,2,0,0,0,97,55.77a1.76,1.76,0,0,0-1.25-.37,1.78,1.78,0,0,0-1.24.37A2,2,0,0,0,94,57.29v3.76a6.17,6.17,0,0,1,1.7-.21,5,5,0,0,1,3.52,1.24,5.15,5.15,0,0,1,1.52,4v2.52a5.27,5.27,0,0,1-1.52,4,5.62,5.62,0,0,1-7,0,5.27,5.27,0,0,1-1.52-4V57.29a5.27,5.27,0,0,1,1.52-4,5.58,5.58,0,0,1,7,0,5.27,5.27,0,0,1,1.52,4v1.64ZM94,66.06v2.52a2,2,0,0,0,.46,1.52,1.78,1.78,0,0,0,1.24.36A1.76,1.76,0,0,0,97,70.1a1.42,1.42,0,0,0,.34-.67,3.1,3.1,0,0,0,.11-.85V66.06a3.1,3.1,0,0,0-.11-.85,1.47,1.47,0,0,0-.34-.67,1.76,1.76,0,0,0-1.25-.36,1.78,1.78,0,0,0-1.24.36A2,2,0,0,0,94,66.06Z"
              fill="#c32729"/>
        </g>
        <a class="gray" id="gray-7" data-name="gray">
          <path
              d="M113.66,80.48H77.36V65.21L75,62.82a.71.71,0,0,1,0-1h0l2.39-2.39V44.17h36.3Z"
              fill="#fff"/>
        </a>
        <path
            d="M117.35,40.51h36.3a40,40,0,0,1-40,40V65.67a.67.67,0,0,1-.14-.11l-2.74-2.74a.71.71,0,0,1,0-1l2.74-2.74.14-.11V44.17a3.66,3.66,0,0,0,3.67-3.66Z"
            fill="none" stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M117.35,40.51h36.3a40,40,0,0,1-40,40V65.67a.67.67,0,0,1-.14-.11l-2.74-2.74a.71.71,0,0,1,0-1l2.74-2.74.14-.11V44.17a3.66,3.66,0,0,0,3.67-3.66Z"
            fill="#fff"/>
        
        <a class="gray" id="gray-8" data-name="gray">
          <path
              d="M117.35,40.51h36.3a40,40,0,0,1-40,40V65.67a.67.67,0,0,1-.14-.11l-2.74-2.74a.71.71,0,0,1,0-1l2.74-2.74.14-.11V44.17a3.66,3.66,0,0,0,3.67-3.66Z"
              fill="#fff"/>
        </a>
        <path
            d="M117.35,40.51h15l2.58,2.58a.71.71,0,0,0,1,0h0l2.59-2.58h15.18a40,40,0,0,0-40-40v36.3a3.67,3.67,0,0,1,3.67,3.67Z"
            fill="none" stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M117.35,40.51h15l2.58,2.58a.71.71,0,0,0,1,0h0l2.59-2.58h15.18a40,40,0,0,0-40-40v36.3a3.67,3.67,0,0,1,3.67,3.67Z"
            fill="#fff"/>
        
        <a class="gray" id="gray-9" data-name="gray">
          <path
              d="M117.35,40.51h15l2.58,2.58a.71.71,0,0,0,1,0h0l2.59-2.58h15.18a40,40,0,0,0-40-40v36.3a3.67,3.67,0,0,1,3.67,3.67Z"
              fill="#fff"/>
        </a>
        <path
            d="M113.66.54H77.36v36.3h36.3V21.91h0l2.73-2.73a.71.71,0,0,0,0-1l-2.73-2.72h0V.54Z"
            fill="none" stroke="#ccc"
            stroke-miterlimit="20"/>
        <path
            d="M113.66.54H77.36v36.3h36.3V21.91l2.73-2.73a.71.71,0,0,0,0-1l-2.73-2.72Z"
            fill="#fff"/>
        
        <a class="gray" id="gray-10" data-name="gray">
          <path
              d="M113.66.54H77.36v36.3h36.3V21.91l2.73-2.73a.71.71,0,0,0,0-1l-2.73-2.72Z"
              fill="#fff"/>
        </a>
        <path
            d="M77.38.54H41.07v36.3H77.38V21.91h0l2.72-2.73a.71.71,0,0,0,0-1l-2.72-2.72h0V.54Z"
            fill="none" stroke="#ccc"
            stroke-miterlimit="20"/>
        <path
            d="M77.38.54H41.07v36.3H77.38V21.91l2.72-2.73a.71.71,0,0,0,0-1l-2.72-2.72Z"
            fill="#fff"/>
        
        <a class="gray" id="gray-11" data-name="gray">
          <path
              d="M77.38.54H41.07v36.3H77.38V21.91l2.72-2.73a.71.71,0,0,0,0-1l-2.72-2.72Z"
              fill="#fff"/>
        </a>
        <path
            d="M41.07.54H5.89A1.12,1.12,0,0,0,4.77,1.65V35.72a1.12,1.12,0,0,0,1.12,1.12H41.07V21.92h0l2.74-2.74a.69.69,0,0,0,0-1h0l-2.74-2.73h0V.54Z"
            fill="none" stroke="#ccc" stroke-miterlimit="20"/>
        <path
            d="M41.07.54H5.89A1.12,1.12,0,0,0,4.77,1.65V35.72a1.12,1.12,0,0,0,1.12,1.12H41.07V21.92l2.74-2.74a.69.69,0,0,0,0-1h0l-2.74-2.73Z"
            fill="#fff"/>
        
        <a class="gray active" id="gray-12" data-name="gray">
          <path
              d="M41.07.54H5.89A1.12,1.12,0,0,0,4.77,1.65V35.72a1.12,1.12,0,0,0,1.12,1.12H41.07V21.92l2.74-2.74a.69.69,0,0,0,0-1h0l-2.74-2.73Z"
              fill="#fff"/>
        </a>
      </svg>
    </a>
    <h2>The Buyability toolkit <sup>TM</sup></h2>
  </div>
</div>
<header>
  <div class="container">
    <section class="steps_title_block">
      <div class="step-title">
        <div class="step-icon-title">
          <div class="step-icon">
            <?= $svg_logo ?>
          </div>
          <div class="line"></div>
          <p class="logo-title"><strong><?= $bold_logo_text ?></strong><?= $light_logo_text ?></p>
        </div>
        <?php $file_snake = ABSPATH . "wp-content/themes/buyablebusiness/template-parts/snake.php";
        include($file_snake);
        ?>
      </div>
    </section>
    <!--    <div class="header-content">-->
    <!--      <a href="--><? //= $ba_logo_link ?><!--"><img alt="--><? //= $ba_logo['alt'] ?><!--" class="logo" src="-->
    <? //= $ba_logo['url'] ?><!--"></a>-->
    <!--      -->
    <!--      <div class="links">-->
    <!--        <a class="small-link" href="http://buyablebusiness.com/" target="_blank">Buyable</a>-->
    <!--        <a class="small-link" href="https://tractionequity.com/steve-preda/" target="_blank">About</a>-->
    <!--      </div>-->
    <!--    </div>-->
  </div>
</header>
<div class="page-wrapper">
  <section class="step firstStep active" id="step-1">
    <div class="container">
      <div class="business-buyAbility">
        <?php the_content(); ?>
        <div class="form-wrapper">
          <div class="container">
            <?php echo do_shortcode($form_shortcode); ?>
            <div class="bottom-buttons">
              <a href="#" class="btn gray-btn btn-medium">Return to the Main Page</a>
              <button type="submit" class="btn btn-medium begin-btn">
                Start the Buyability Assessment
                <svg height="8" viewBox="0 0 5 8" width="5" xmlns="http://www.w3.org/2000/svg">
                  <g>
                    <g>
                      <path d="M2.313-.063l2.756 3.84v.188L2.312 7.806H.458l2.358-3.931L.458-.063z" fill="#fff"></path>
                    </g>
                  </g>
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="step-wrapper">
    <div class="container">
      <div class="phases-and-steps">
        <div class="phases" style="display: none;">
          <div class="number active">1</div>
          <div class="number number-2">2
            <div class="border-top"></div>
          </div>
          <div class="number number-2">3
            <div class="border-top"></div>
          </div>
          <div class="number number-2">4
            <div class="border-top"></div>
          </div>
          <div class="number number-2">5
            <div class="border-top"></div>
          </div>
          <div class="number number-2">6
            <div class="border-top"></div>
          </div>
          <div class="number number-2">7
            <div class="border-top"></div>
          </div>
          <div class="number number-2">8
            <div class="border-top"></div>
          </div>
        </div>
        <div class="steps">
          <div class="step secondStep active" id="step-2">
            <div class="question-and-evaluation">
              <div class="loading-wrapper" id="loading">
                <div class="sp sp-circle"></div>
              </div>
              <div class="question">
                <div id="progress-container">
                  <div class="progress"></div>
                </div>
                <h1 class="headline-1 ques-title"></h1>
                <p class="paragraph ques-body"></p>
                <div class="buttons">
                  <a class="btn btn-small prev-ques hide" href="#">Back</a>
                  <a class="btn btn-medium next-ques" href="#">
                    Next
                    <svg height="8" viewBox="0 0 5 8" width="5" xmlns="http://www.w3.org/2000/svg">
                      <g>
                        <g>
                          <path d="M2.313-.063l2.756 3.84v.188L2.312 7.806H.458l2.358-3.931L.458-.063z" fill="#fff"/>
                        </g>
                      </g>
                    </svg>
                  </a>
                  <a class="btn btn-small clear_values" style="display:none;" onclick="clear_values_circles();" href="#">Clear</a>
                </div>
              </div>
              <div class="evaluation">
                <div class="evaluation-number">
                  <div class="fill"></div>
                  <div class="circle filled_submit" data-number="5">5</div>
                  <div class="fill"></div>
                  <div class="circle filled_submit" data-number="4">4</div>
                  <div class="fill"></div>
                  <div class="circle filled_submit" data-number="3">3</div>
                  <div class="fill"></div>
                  <div class="circle filled_submit" data-number="2">2</div>
                  <div class="fill"></div>
                  <div class="circle filled_submit" data-number="1">1</div>
                  <div class="text">
                    <h2 class="headline-2 strong">strong</h2>
                    <h2 class="headline-2 weak">weak</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="step thirdStep" id="step-3">
            <div class="result">
              <h1 class="headline-1" id="step-3-title"></h1>
              <div id="html2canvas">
                <p class="paragraph text email">john.doe@gmail.com</p>
                <p class="total-headline">Your Businessâ€™ Overall Buyability Strength:</p>
                <svg class="total-gear" viewBox="0 0 293.4 293.4">
                  <circle class="gear-circle-white" clip-path="url(#gear-clip-path)"
                          fill="none"
                          stroke="#ffffff" stroke-width="45" stroke-miterlimit="10" cx="146.7" cy="146.7" r="124.1"/>
                  <circle class="gear-circle" clip-path="url(#gear-clip-path)"
                          fill="none"
                          stroke="#c32729" stroke-width="45" stroke-miterlimit="10" cx="146.7" cy="146.7" r="124.1"/>
                  
                  <path class="gear-stroke" fill="none" stroke="#c32729" stroke-width="2" stroke-miterlimit="10" d="M165.4,27.5
  c8.4,1.3,16.7,3.5,24.7,6.6l13.2-17.4c1.4-1.9,4-2.4,6-1.2L229,26.8c2,1.2,2.9,3.7,2,5.8l-8.5,20.2c6.6,5.4,12.7,11.4,18,18
  l20.2-8.5c2.2-0.9,4.6-0.1,5.8,2L278,84.1c1.2,2,0.7,4.6-1.2,6l-17.4,13.2c3.1,8,5.3,16.2,6.6,24.7l21.7,2.8
  c2.3,0.3,4.1,2.3,4.1,4.6v22.8c0,2.3-1.7,4.3-4.1,4.6l-21.7,2.8c-1.3,8.4-3.5,16.7-6.6,24.7l17.4,13.2c1.9,1.4,2.4,4,1.2,6
  L266.6,229c-1.2,2-3.7,2.9-5.8,2l-20.2-8.5c-5.4,6.6-11.4,12.7-18,18l8.5,20.2c0.9,2.2,0.1,4.6-2,5.8L209.3,278
  c-2,1.2-4.6,0.7-6-1.2l-13.2-17.4c-8,3.1-16.2,5.3-24.7,6.6l-2.7,21.7c-0.3,2.3-2.3,4.1-4.6,4.1h-22.8c-2.3,0-4.3-1.7-4.6-4.1
  l-2.8-21.7c-8.4-1.3-16.7-3.5-24.7-6.6l-13.2,17.4c-1.4,1.9-4,2.4-6,1.2l-19.7-11.4c-2-1.2-2.9-3.7-2-5.8l8.5-20.2
  c-6.6-5.4-12.7-11.4-18-18L32.6,231c-2.2,0.9-4.6,0.1-5.8-2l-11.4-19.7c-1.2-2-0.7-4.6,1.2-6l17.4-13.2c-3.1-8-5.3-16.2-6.6-24.7
  l-21.7-2.8c-2.3-0.3-4.1-2.3-4.1-4.6v-22.8c0-2.3,1.7-4.3,4.1-4.6l21.7-2.8c1.3-8.4,3.5-16.7,6.6-24.7L16.6,90.1
  c-1.9-1.4-2.4-4-1.2-6l11.4-19.7c1.2-2,3.7-2.9,5.8-2l20.2,8.5c5.4-6.6,11.4-12.7,18-18l-8.5-20.2c-0.9-2.2-0.1-4.6,2-5.8L84,15.4
  c2-1.2,4.6-0.7,6,1.2l13.2,17.4c8-3.1,16.2-5.3,24.7-6.6l2.8-21.7c0.3-2.3,2.3-4.1,4.6-4.1h22.8c2.3,0,4.3,1.7,4.6,4.1L165.4,27.5z"/>
                  
                  <path class="gear-inner-circle" fill="#F0EFEB" d="M146.7,249.2c56.5,0,102.5-46,102.5-102.5c0-56.5-46-102.5-102.5-102.5
  S44.2,90.2,44.2,146.7C44.2,203.1,90.2,249.2,146.7,249.2z"/>
                  <g class="gear-text" y="50%" data-svg-origin="97 55.359375" transform="matrix(1,0,0,1,0,20.4375)" style="transform-origin: 0px 0px;">
                    <text class="svg-percentage" x="77" y="160" fill="#c32729" style="font-size: 90px;">99</text>
                    <text class="svg-percentage" x="180" y="120" fill="#c32729" style="font-size: 50px;">%</text>
                  </g>
                </svg>
                <div class="gears-container"></div>
              </div>
              <div class="tabsWrapper" id="tabs">
                <div class="tabNav"></div>
                <div class="tabContentWrapper"></div>
              
              </div>
              <a class="btn btn-small next-category">Reset</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="footer-content">
        <div class="social-icons">
          <a class="icon facebook" href="<?= $ba_facebook_link ?>">
            <svg height="30" viewBox="0 0 15 30" width="15" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path d="M9.708 8.96V6.6c0-1.151.767-1.418 1.306-1.418h3.312V.122L9.764.103c-5.063 0-6.214 3.775-6.214 6.19V8.96H.622v5.904h2.953v14.763h5.904V14.865h4.383l.212-2.318.326-3.586z"
                        fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
          <a class="icon twitter" href="<?= $ba_twitter_link ?>">
            <svg height="24" viewBox="0 0 30 24" width="30" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path
                      d="M29.677 3.19a12.214 12.214 0 0 1-3.478.938 6.006 6.006 0 0 0 2.664-3.3 12.23 12.23 0 0 1-3.85 1.45A6.086 6.086 0 0 0 20.596.393c-3.345 0-6.057 2.67-6.057 5.963 0 .467.054.92.155 1.358a17.286 17.286 0 0 1-12.484-6.23 5.874 5.874 0 0 0-.822 2.997c0 2.068 1.072 3.894 2.695 4.963A6.115 6.115 0 0 1 1.338 8.7v.075c0 .763.15 1.491.415 2.161a6.047 6.047 0 0 0 4.445 3.688 6.253 6.253 0 0 1-1.597.21c-.39 0-.768-.039-1.14-.11.773 2.37 3.01 4.094 5.659 4.142a12.27 12.27 0 0 1-7.523 2.553c-.49 0-.971-.028-1.447-.083a17.368 17.368 0 0 0 9.287 2.678c9.469 0 15.289-6.56 16.823-13.365a16.37 16.37 0 0 0 .396-4.37 12.191 12.191 0 0 0 3.02-3.088z"
                      fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
          <a class="icon linked-in" href="<?= $ba_linkedin_link ?>">
            <svg height="31" viewBox="0 0 30 31" width="30" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path d="M.342 15.831v14.46h6.89V10.606H.342z" fill="#fefefe"/>
                </g>
                <g>
                  <path d="M3.788.766a3.445 3.445 0 1 0 0 6.89 3.445 3.445 0 0 0 0-6.89z" fill="#fefefe"/>
                </g>
                <g>
                  <path
                      d="M29.717 16.65c-.467-3.71-2.17-6.042-7.186-6.042-2.975 0-4.973 1.105-5.79 2.656h-.085v-2.656H11.17V30.29h5.735v-9.756c0-2.573.487-5.064 3.668-5.064 3.137 0 3.39 2.942 3.39 5.23v9.59h5.905V19.474h.001c0-1.008-.042-1.953-.15-2.823z"
                      fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
          <a class="icon youtube" href="<?= $ba_youtube_link ?>">
            <svg height="22" viewBox="0 0 30 22" width="30" xmlns="http://www.w3.org/2000/svg">
              <g>
                <g>
                  <path
                      d="M20.004 10.551l-7.974 4.308-.003-6.632V6.21l3.597 1.957zm8.367-8.898C27.248.428 25.989.42 25.414.35 21.28.04 15.085.04 15.085.04h-.015S8.871.04 4.741.351C4.164.42 2.908.428 1.782 1.653.899 2.587.611 4.711.611 4.711S.314 7.2.314 9.692v2.335c0 2.493.297 4.984.297 4.984s.288 2.122 1.171 3.055c1.126 1.227 2.602 1.187 3.257 1.315 2.362.236 10.038.31 10.038.31s6.203-.011 10.337-.321c.575-.072 1.834-.077 2.957-1.305.884-.933 1.172-3.054 1.172-3.054s.297-2.491.297-4.984V9.692c0-2.492-.297-4.982-.297-4.982s-.288-2.124-1.172-3.057z"
                      fill="#fefefe"/>
                </g>
              </g>
            </svg>
          </a>
        </div>
        <div class="links">
          <?= $ba_footer_text ?>
        </div>
      </div>
    </div>
  </footer>
  <?php wp_footer(); ?>
  <script src="<?= plugin_dir_url(__FILE__) . '/assets/js/gsap.min.js' ?>"></script>
  <script src="<?= plugin_dir_url(__FILE__) . '/assets/js/drawSVGPlugin.js' ?>"></script>
  <script src="<?= plugin_dir_url(__FILE__) . '/assets/js/scripts.js' ?>"></script>
</div>

<script>
  //next-ques
  function clear_values_circles() {
    jQuery('.circle').removeClass('active');
    var url = '<?= admin_url('admin-ajax.php'); ?>';
    jQuery.ajax({
      url: url,
      type: 'POST',
      data: {
        page: 'buyabilityassessment',
        action: 'clear_questions',
      },
      success: function (data, status) {
        window.location = '<?= site_url() . '/buyabilityassessment'; ?>';
      },
      error: function (xhr, desc, err) {
      }
    });
  }
  
  function save_question_for_user(question, answer) {
    
    var url = '<?= admin_url('admin-ajax.php'); ?>';
    jQuery.ajax({
      url: url,
      type: 'POST',
      data: {
        page: 'buyabilityassessment',
        question: question,
        active_circles: answer,
        action: 'save_question_for_users',
      },
      success: function (data, status) {
        // jQuery('.next-ques').show();
        // console.log('saved:', answer, question);
      },
      error: function (xhr, desc, err) {
      }
    });
  }
  
  
  function check_circles() {
    var question = jQuery('.paragraph.ques-body').html();
    var url = '<?= admin_url('admin-ajax.php'); ?>';
    
    jQuery.ajax({
      url: url,
      type: 'POST',
      data: {
        page: 'buyabilityassessment',
        question: question,
        action: 'get_question_for_users',
      },
      success: function (data, status) {
        var json = JSON.parse(data);
        const answer = json[question];
        // console.log('current_question:', answer, question);
        answer && jQuery(`.circle[data-number="${answer}"]`).click();
        // answer && jQuery('.clear_values').show();
      },
      error: function (xhr, desc, err) {
      }
    });
    
  }
  
  
  function check_circle_click() {
  
  }
  
  jQuery('.filled_submit').click(function (data) {
setTimeout(()=>{
  
  var question = jQuery('.paragraph.ques-body').html();
  var answer = jQuery('.circle.active').length;
  
  save_question_for_user(question, answer);
},100)
  });
  
  jQuery('.next-ques, .prev-ques, .next-category').click(function (data) {
    // jQuery('.next-ques').hide();
    // jQuery('.circle').removeClass('active');
    check_circles();
  });
  
  
  jQuery('.begin-btn').click(function (data) {
    setTimeout(function () {
      
      check_circles();
      
    }, 1000);
    
    
  });
  
  jQuery(document).ready(function () {
    check_circles();
  });
</script>


</body>
</html>